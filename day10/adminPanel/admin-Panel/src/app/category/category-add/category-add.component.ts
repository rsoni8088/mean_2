import { CategoryService } from './../category.service';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-category-add',
  templateUrl: './category-add.component.html',
  styleUrls: ['./category-add.component.css']
})
export class CategoryAddComponent implements OnInit {

  title: string = ''
  description: string = ''

  constructor(
    private activeModel: NgbActiveModal,
    private service: CategoryService
  ) { }

  ngOnInit(): void {
  }

  onSave() {

    this.service.createCategory(this.title, this.description).subscribe(response => {

      if (response['status'] == "SUCCESS") {
        this.activeModel.dismiss("OK")
      } else {
        alert(response['error'])
      }
    })

  }
  onCancel() {
    this.activeModel.close("OK")
  }

}
