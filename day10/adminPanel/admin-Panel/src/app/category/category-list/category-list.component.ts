import { CategoryService } from './../category.service';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoryAddComponent } from '../category-add/category-add.component';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {

  categories = []

  constructor(
    private model: NgbModal,
    private service: CategoryService) { }

  ngOnInit(): void {
    this.getCategories()
  }


  getCategories() {
    this.service.getCategories().subscribe(response => {

      if (response['status'] == "SUCCESS") {
        this.categories = response['data']
      } else {
        alert(response['error'])
      }
    });
  }

  onAdd() {
    const ref = this.model.open(CategoryAddComponent);
    ref.result.finally(() => {
      this.getCategories()
    })
  }

  OnDelete(category) {
    console.log(category)
    this.service.deleteCategory(category._id).subscribe(response => {

      if (response['status'] == "SUCCESS") {
        this.getCategories();
      } else {
        alert(response['error'])
      }
    });
  }

}
