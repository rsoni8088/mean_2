import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http"

@Injectable({
  providedIn: 'root'
})
export class CategoryService {


  url = "http://localhost:4000/categories/";

  constructor(
    private http: HttpClient
  ) { }

  getCategories() {
    const token = sessionStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        token: token
      })
    }

    return this.http.get(this.url, httpOptions);
  }

  createCategory(title: string, description: string) {
    const token = sessionStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        token: token
      })
    }

    const body = {
      title,
      description
    }
    return this.http.post(this.url, body, httpOptions);
  }


  deleteCategory(id: string) {
    console.log(id)
    const token = sessionStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        token: token
      })
    }
    return this.http.delete(this.url + "/" + id, httpOptions)
  }


}
