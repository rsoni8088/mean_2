import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductService {


  url = "http://localhost:4000/products/";

  constructor(
    private http: HttpClient
  ) { }

  getProducts() {
    const token = sessionStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        token: token
      })
    }

    return this.http.get(this.url, httpOptions);
  }

  createProducts(title: string, description: string, price: number, category: any) {
    const token = sessionStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        token: token
      })
    }

    const body = {
      title,
      description,//
      price, //
      category
    }
    return this.http.post(this.url, body, httpOptions);
  }


  deleteProducts(id: string) {
    console.log(id)
    const token = sessionStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        token: token
      })
    }
    return this.http.delete(this.url + "/" + id, httpOptions)
  }

  uploadImage(_id: any, selectImage: any) {
    const token = sessionStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        token: token
      })
    }

    const body = new FormData();
    body.append("image", selectImage)
    return this.http.post(this.url + "upload-image/" + _id, body, httpOptions);
  }

}
