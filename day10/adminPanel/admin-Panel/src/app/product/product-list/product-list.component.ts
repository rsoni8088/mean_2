import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProductAddComponent } from '../product-add/product-add.component';
import { ProductService } from '../product.service';
import { UploadProductComponent } from '../upload-product/upload-product.component';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products = []

  constructor(
    private model: NgbModal,
    private service: ProductService) { }

  ngOnInit(): void {
    this.getProducts()
  }


  getProducts() {
    this.service.getProducts().subscribe(response => {

      if (response['status'] == "SUCCESS") {
        this.products = response['data']
      } else {
        alert(response['error'])
      }
    });
  }

  onAdd() {
    const ref = this.model.open(ProductAddComponent);
    ref.result.finally(() => {
      this.getProducts()
    })
  }

  OnDelete(product) {
    this.service.deleteProducts(product._id).subscribe(response => {
      if (response['status'] == "SUCCESS") {
        this.getProducts();
      } else {
        alert(response['error'])
      }
    });
  }


  OnUploadImage(product) {
    const ref = this.model.open(UploadProductComponent)
    const component = ref.componentInstance as UploadProductComponent;
    component.product = product
    ref.result.finally(() => {
      this.getProducts()
    })
  }


}
