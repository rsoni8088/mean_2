import { ProductService } from './../product.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-upload-product',
  templateUrl: './upload-product.component.html',
  styleUrls: ['./upload-product.component.css']
})
export class UploadProductComponent implements OnInit {


  product: any;
  selectImage: any;


  constructor(
    private productService: ProductService,
    private ngbActiveModal: NgbActiveModal) { }

  onImageSelected(event) {
    console.log(event)

    this.selectImage = event.target.files[0]

  }

  onUpload() {
    this.productService.uploadImage(this.product._id, this.selectImage).subscribe(response => {
      if (response['status'] == "SUCCESS") {
        this.ngbActiveModal.dismiss()
      } else {
        alert(response['error'])
      }
    });


  }
  onCancel() {
    this.ngbActiveModal.close("ok");
  }


  ngOnInit(): void {
  }

}
