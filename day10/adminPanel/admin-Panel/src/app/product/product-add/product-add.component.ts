import { CategoryService } from './../../category/category.service';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {

  title: string = ''
  description: string = ''
  price: number
  category : string
  categories: []


  constructor(

    private activeModel: NgbActiveModal,
    private service: ProductService,
    private categoryService: CategoryService
  ) {


  }

  loadCategories() {
    this.categoryService
      .getCategories()
      .subscribe(response => {
        if (response['status'] == 'SUCCESS') {
          this.categories = response['data']
        } else {
          alert(response['error'])
        }
      })
  }

  ngOnInit(): void {
    this.loadCategories()
  }

  onSave() {
    this.service.createProducts(this.title, this.description, this.price, this.category)//
      .subscribe(response => {
        if (response['status'] == "SUCCESS") {
          this.activeModel.dismiss("OK")
        } else {
          alert(response['error'])
        }
      })

  }
  onCancel() {
    this.activeModel.close("OK")
  }

}
