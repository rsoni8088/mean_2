import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {


  firstName: string
  lastName: string
  email: String
  password: string
  phone: string


  constructor(
    private router: Router,
    private authService: AuthService) { }

  ngOnInit(): void {
  }

  onSignUp() {
    this.authService//
      .signUp(this.firstName, this.lastName, this.email, this.password, this.phone)//
      .subscribe(response => {
        console.log(response)
        if (response['status'] == "SUCCESS") {
          console.log("SUCCESS")
          this.router.navigate(['/auth/signin'])
        } else {
          console.log("ERROR")
          alert(response['error'])
        }
      })
  }

}
