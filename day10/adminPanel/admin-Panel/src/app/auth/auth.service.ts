import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {

  url = "http://localhost:4000/admins";

  constructor(
    private http: HttpClient
  ) { }


  // Auth guards
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (sessionStorage.getItem('token')) {
      return true;
    }

    return false;

  }

  signUp(firstName: string, lastName: string, email: String, password: string, phone: string) {
    const body = {
      firstName,
      lastName,
      email,
      password,
      phone
    }
    return this.http.post(this.url + '/signup', body)

  }
  signin(email: String, password: string) {
    const body = {
      email: email,
      password: password
    }
    return this.http.post(this.url + '/signin', body)
  }
}
