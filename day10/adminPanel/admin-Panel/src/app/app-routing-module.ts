import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthService } from './auth/auth.service';

const routes: Routes = [

    { path: '', redirectTo : "/home" , pathMatch : "full" },


    // These 3 compoment loaded inside HOME component as they are pages after login or sign up successfull
    {
        path: "home",//
        // This is used to check if user is logged in then only it is going to serve page
        // Using Guards
        canActivate : [AuthService],
        component: HomeComponent,
        children: [
            { path: 'product', loadChildren: () => import("./product/product.module").then(m => m.ProductModule) },
            { path: 'category', loadChildren: () => import("./category/category.module").then(m => m.CategoryModule) },
            { path: 'user', loadChildren: () => import("./user/user.module").then(m => m.UserModule) }
        ]
    },

    // Here we are loading the entire module which contains multiple components
    // Load the child module / e.g. - auth/singup
    { path: 'auth', loadChildren: () => import("./auth/auth.module").then(m => m.AuthModule) }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }
