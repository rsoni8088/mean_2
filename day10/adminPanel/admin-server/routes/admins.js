const express = require("express");
const crypto = require("crypto-js");
const jwt = require("jsonwebtoken");

const config = require("../config");
const Admin = require("../models/Admin");
const utils = require("../utils");

const router = express.Router();

router.post("/signup", (request, response) => {
  console.log(request.body);
  const { firstName, lastName, password, email, phone } = request.body;

  const admin = new Admin();
  admin.firstName = firstName;
  admin.lastName = lastName;
  admin.password = crypto.MD5(password);
  admin.phone = phone;
  admin.email = email;

  // Admin collections will be created inside mongoDb named as Admins
  admin.save((error, admin) => {
    response.send(utils.createResult(error, admin));
  });
});

router.post("/signin", (request, response) => {
  console.log(request.body);
  const { password, email } = request.body;

  // Because it is encrpted password then we do need to convert it into String
  const encryptedPassword = "" + crypto.MD5(password);
  Admin.findOne(
    {
      email: email,
      password: encryptedPassword,
    },
    { __v: 0, deleted: 0, createdTimeStamp: 0, _id: 0, password: 0 }
  ) //
    .exec((error, admin) => {
      if (error) {
        response.send(utils.createResult(error, null));
      } else if (!admin) {
        response.send(utils.createResult("Invalid Email and Password", null));
      } else {
        const payload = {
          id: admin._id,
          name: admin.firstName + " " + admin.lastName,
        };
        const token = jwt.sign(payload, config.secret);
        response.send(
          utils.createResult(error, {
            name: admin.firstName + " " + admin.lastName,
            email: admin.email,
            phone: admin.phone,
            token: token,
          })
        );
      }
    });
});

module.exports = router;
