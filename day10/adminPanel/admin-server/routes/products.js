const express = require("express");
const utils = require("../utils");

// It is used for uploading the image or any other doceument
const multer = require("multer");
const upload = multer({ dest: "images/" });

const fs = require("fs");

const Product = require("../models/product");

const router = express.Router();
router.post(
  "/upload-image/:id",
  upload.single("image"),
  (request, response) => {
    const { id } = request.params;

    Product.findOne({ _id: id, deleted: false }).exec((error, product) => {
      if (error) {
        response.send(utils.createResult(error, null));
      } else if (!product) {
        response.send(utils.createResult("category not found", null));
      } else {
        console.log(product);
        product.img = request.file.filename;
        product.save((error, product) => {
          response.send(utils.createResult(error, product));
        });
      }
    });
  }
);

router.get("/image/:fileName", (request, response) => {
  const { fileName } = request.params;
  const path = __dirname + "/../images/" +fileName 
  console.log(path)
  fs.readFile(path , (error, data)=>{
    response.send(data)
  })
});

router.post("/", (request, response) => {
  const { title, description, category, price } = request.body;
  const product = new Product();
  product.title = title;
  product.description = description;
  product.category = category;
  product.price = price;

  product.save((error, product) => {
    if (error) {
      response.send(utils.createResult(error, null));
    } else {
      response.send(utils.createResult(null, product));
    }
  });
});

router.get("/", (request, response) => {
  Product.find({ deleted: false }, {}) //
    .populate("category", "title _id") //
    .exec((error, products) => {
      response.send(utils.createResult(error, products));
    });
});

router.put("/:id", (request, response) => {
  const { id } = request.params;
  const { title, description, category, price } = request.body;

  Product.findOne({ _id: id, deleted: false }) //
    .exec((error, product) => {
      if (error) {
        response.send(utils.createResult(error, null));
      } else if (!product) {
        response.send(utils.createResult("Product not found", null));
      } else {
        product.title = title;
        product.description = description;
        product.category = category;
        product.price = price;

        product.save((error, product) => {
          response.send(utils.createResult(error, product));
        });
      }
    });
});

router.delete("/:id", (request, response) => {
  const { id } = request.params;

  Product.findOne({ _id: id }) //
    .exec((error, product) => {
      if (error) {
        response.send(utils.createResult(error, null));
      } else if (!product) {
        response.send(utils.createResult("Product not found", null));
      } else {
        product.deleted = true;
        product.save((error, product) => {
          response.send(utils.createResult(error, product));
        });
      }
    });
});

module.exports = router;
