const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const jwt = require("jsonwebtoken");

const config = require("./config");
const utils = require("./utils");

const mongoose = require("mongoose");
mongoose.connect(config.dbConnection);

const app = express();
app.use(bodyParser.json());
app.use(cors("*"));

app.use((request, response, next) => {
  if (
    request.url == "/admins/signup" ||
    request.url == "/admins/signin" ||
    request.url.startsWith("/products/image")
  ) {
    next();
  } else {
    try {
      const token = request.headers["token"];
      if (!token) {
        response.send(utils.createResult("Unauthorized"));
        return;
      }

      const payload = jwt.verify(token, config.secret);
      request.userId = payload.id;
      next();
    } catch (ex) {
      response.send(utils.createResult("Unauthorized"));
    }
  }
});

const routerUsers = require("./routes/users");
const routerOrders = require("./routes/orders");
const routerProducts = require("./routes/products");
const routerAdmins = require("./routes/admins");
const routerCategory = require("./routes/categories");

app.use("/users", routerUsers);
app.use("/orders", routerOrders);
app.use("/products", routerProducts);
app.use("/admins", routerAdmins);
app.use("/categories", routerCategory);

app.get("/", (request, response) => {
  response.send("Welcome Admin Panel web API's");
});

app.listen(4000, "0.0.0.0", () => {
  console.log("Application server started on PORT 4000");
});
