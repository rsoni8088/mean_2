const mongoose = require("mongoose")
const { Schema } = mongoose;

const ProductSchema = new Schema({
  title: { type: String, trim: true, required: true },
  description: { type: String, trim: true, required: true },
  price: { type: Number, required: true },
  // referring to category schema
  category: { type: mongoose.Schema.Types.ObjectId, ref: "Category" },
  img: { type: String, trim: true, required: false },
  rating: [
    {
      userId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
      rating: { type: Number, required: true },
      comment: { type: String, trim: true, required: false },
    },
  ],

  isActive: { type: Boolean, default: false },
  deleted: { type: Boolean, default: false },
  createdTimeStamp: { type: Date, default: new Date() },
});

module.exports = mongoose.model("Product", ProductSchema);
