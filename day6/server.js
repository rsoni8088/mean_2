const express = require("express");

const bodyParser = require("body-parser");

const app = express();

app.use(bodyParser.json());
const routerEmp = require("./routes/emp");

app.use("/emp", routerEmp);

app.listen(4000, "0.0.0.0", () => {
  console.log("Server started on PORT 4000");
});
