function createResult(error, data) {
  const result = {};
  if (error) {
    result["ERROR"] = "ERROR";
    result["error"] = error;
  } else {
    result["SUCCESS"] = "SUCCESS";
    result["data"] = data;
  }
  return result;
}

module.exports = {
  createResult: createResult,
};
