function function1(params) {
  const numbers = [10, 20, 30, 40, 50, 60];
  console.log(numbers);

  numbers.push(70);
  console.log(numbers);

  numbers.pop();
  console.log(numbers);

  numbers.splice(2, 2);
  console.log(numbers);

  console.log(`is 10 present = ${numbers.includes(10)}`);
  console.log(`is 30 present = ${numbers.includes(300)}`); // gives boolean value

  console.log(`10 present at index = ${numbers.indexOf(10)}`);
  console.log(`30 present at index = ${numbers.indexOf(300)}`);


  console.log(numbers.join(","))
}

 //function1()

function function2(params) {
  const numbers = [1, 2, 3, 4, 5];

  // calls parameter function for every values within the array
  const squares = numbers.map(function (number) {
      // return Math.pow(number, 2)
    return number * number;
  }); 
  console.log(numbers)
  console.log(squares)
}

//function2()


function function3(params) {
    const numbers = [1, 2, 3, 4, 5];
    const triple = numbers.map( (number) =>{
      return number ** 3;
    });
    console.log(numbers)
    console.log(triple)
  }
  
 // function3()



