// create object using JSON
// Javascript object notation
const person = {
    name : "Rishabh",
    address : "Pune",
    age : 30,
    email : "rsoni8088@gmail.com"

}

console.log(person)


// create object using object function which is in built function
const mobile = new Object()
// Adding new properties
mobile.model = "iPhone XS"
mobile.price = "13333"

console.log(mobile)


// Using constructor function
function Car(model, company, price) {

    // Adding new properties
    this.model =model
    this.company =company
    this.price  = price



}

const car1 = new  Car("i20","hyndai", 7.5)
console.log(car1)

const car2 = new  Car("i10","hyndai", 7)
console.log(` Model of car = ${car2.model}`)