import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {

  public userName: string = "John"
  public address: string = "India"
  public age: number = 40
  public phone: string = "+91-9673144923"

  public product = {
    title: "Title 1",
    description: "Description",
    price: 100,
    category: "Testing"
  }

  constructor() { }

  ngOnInit(): void {
  }

}
