// Functional programming aspect
// Array and Loops

// 1. Traditional for loop
function function1() {
  const numbers = [10, 20, 30, 40, 50, 60, 70, 80];

  for (let index = 0; index < numbers.length; index++) {
    const element = numbers[index];
    console.log(`Square of ${element} is ${element * element}`);
  }
}

//function1();

// 2. for of loop
function function2() {
  const numbers = [10, 20, 30, 40, 50, 60, 70, 80];
  for (const element of numbers) {
    console.log(`Square of ${element} is ${element * element}`);
  }
}

//function2();

// 3. for each loop
function function3() {
  const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  numbers.forEach((element) => {
    console.log(`Square of ${element} is ${element * element}`);
  });

  // find even numbers
  const evens = numbers.filter((value) => {
    if (value % 2 == 0) {
      console.log(`Values, Which are even ${value}`);
    }
  });
  console.log(evens);
}

//function3();

function function4() {
  const numbers = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
  const sum = numbers.reduce((p1, p2) => {
    return p1 + p2
  });
  console.log(sum);
}

function4();
