const os = require("os");

console.log(`Windows = ${os.platform()}`);
console.log(`cpu architecture = ${os.arch()}`);

console.log(`total memory = ${os.totalmem() / (1024 * 1024 * 1024)} GB`);
console.log(`free memory = ${os.freemem() / (1024 * 1024 * 1024)} GB`);

const cpus = os.cpus();
for (const cpu of cpus) {
  console.log(`${cpu.model} - ${cpu.speed}`);
}

console.log(`Home directory = ${os.homedir()}`);
console.log(`Hostname = ${os.hostname()}`);
