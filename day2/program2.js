function function1(p1) {
  // P1 is function3 when we pass function 3
  console.log(`p1 is = ${p1}, type= ${typeof p1}`);
}

// function1(10)
// function1("Sunny")
// function1(true)

// Function alias
// Function pointer
const function2 = function1;

// function2(1000)

const function3 = () => {
  console.log(" inside function3");
};

function1(function3);

function execute(func) {
  console.log("inside execute");
  console.log(func);
  console.log(`type of =${typeof func}`);
  func(10, 20);
}

function add(p1, p2) {
  console.log(`add =${p1 + p2}`);
}

function seprate(p1, p2) {
  console.log(`separate =${p1 - p2}`);
}

// execute(add);
// execute(seprate);

const divide = (p1, p2) => {
  console.log(`divide = ${p1 / p2}`);
};

execute(divide);

execute((p1, p2) => {
  console.log(`Multiply =${p1 * p2}`);
});


// Backback functions - all above functions are callback functions
// We never call them by name
