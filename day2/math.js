// Module
// Collection of funtions/ librarby 

function add_func(p1, p2) {
  console.log(`Addition is = ${p1 + p2}`);
}

function sub_func(p1, p2) {
  console.log(`Substract is = ${p1 - p2}`);
}

function divide_func(p1, p2) {
  console.log(`Divison is = ${p1 / p2}`);
}

function multiply_func(p1, p2) {
  console.log(`Multiplication is = ${p1 * p2}`);
}

//console.log(module)


//  Export the different entities
module.exports = {
    add : add_func,
    sub : sub_func,
    divide : divide_func,
    multiply : multiply_func
}

// add(10, 20);
// sub(30, 10);
// divide(20, 10);
// multiply(10, 2);
