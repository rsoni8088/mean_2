function function1() {
  const person1 = {
    name: "Sunny",
    age: 24,
    address: "raisen",
  };

  // Other way araound

  // const person2 = new Object()
  // person2.name =person1.name
  // person2.age =person1.age
  // person2.address =person1.address
  // person2.mobile = "+9673144923"

  // Spread or rest operator
  const person2 = {
    ...person1,
    phone: "+91-9673144923",
  };

  console.log(person1);
  console.log(person2);
}

// function1()
// spread(...) operator on array
function function2() {
  const numbers = [1, 2, 3, 4, 5];
  const latestNumbers = [...numbers, 6, 7, 8, 9, 10];
  console.log(`Old numbers = ${numbers}`);
  console.log(`Latest numbers = ${latestNumbers}`);
}
function2();



// variable length arguments function
// rest operator
function add(...args) {
  const addition = args.reduce((x, y) => {
    return x + y;
  });
  console.log(`Addition is ${addition}`);
}

add(10,20)
add(10,20,30,40)