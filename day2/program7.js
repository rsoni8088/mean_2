// import module

const fs = require("fs");


// Sync 
// BLOCKING API
function syncReadFile() {
  try {
    console.log("File reading started");

    // Step 1
    const data = fs.readFileSync("./myFile.txt");

    //step 2
    console.log("file read completed");

    // step 3
    console.log(`Data ${data}`);
  } catch (ex) {
    console.log(ex.errno);
  }
  // step 4
  console.log("Execution started for math calculation");

  // step 5
  const result = 1234454635356e64 * 824678217462162746;

  // step 6
  console.log(`result ${result}`);

  // step 7
  console.log("Execution completed for math calculation");
}

syncReadFile();


console.log("----------------------------------------------------------");

// Asyc behavior

// NON BLOCKING API
function asyncFileRead() {
  console.log("File reading started");

  fs.readFile("./myFile1.txt", (error, data) => {
    console.log("File read completed");
    if (error) {
      console.log(error.errno);
    } else {
      console.log(`data - ${data}`);
    }
  });

  // step 4
  console.log("Execution started for math calculation");

  // step 5
  const result = 1234454635356e64 * 824678217462162746;

  // step 6
  console.log(`result ${result}`);

  // step 7
  console.log("Execution completed for math calculation");
}

asyncFileRead();
