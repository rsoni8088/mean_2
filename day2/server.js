// import http module
const http = require("http");

// create a server object
const server = http.createServer((request, response) => {
  console.log("request receive");

  // process the request
  console.log(`path - ${request.url}`);
  console.log(`path - ${request.method}`);

  if (request.url == "/product") {
      console.log("Inside this product" )
    // execute product query
    if (request.method == "GET") {
      console.log("select * from product ....");
    } else if (request.method == "POST") {
      console.log("insert into product values ...");
    } else if (request.method == "PUT") {
      console.log("update product set ...");
    } else if (request.method == "DELETE") {
      console.log("delete product where ...");
    }
  } else if (request.url == "/user") {

    // execute user query
    if (request.method == "GET") {
      console.log("select * from user ....");
    } else if (request.method == "POST") {
      console.log("insert into user values ...");
    } else if (request.method == "PUT") {
      console.log("update user set ...");
    } else if (request.method == "DELETE") {
      console.log("delete user where ...");
    }
  }

  // send the response
  response.end();
});

// start the server
server.listen(4000, "0.0.0.0", () => {
  console.log("server start on port 4000");
});
