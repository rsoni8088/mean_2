// Destructuring

function function1() {
  const person = {
    name: "Rishabh",
    age: 10,
    email: "rsoni8088@gmail.com",
  };
  console.log(`name : ${person.name}`);
  console.log(`age : ${person.age}`);
  console.log(`email : ${person.email}`);

  const { name, age, email } = person;
  console.log(`name : ${name}`);
  console.log(`age : ${age}`);
  console.log(`email : ${email}`);
}

function1();

function function2() {
  const countries = ["india", "usa", "uk", "japan"];
  // console.log(countries)

  // sequence matters a lot
  const [c1, c2, c3, c4] = countries;
  console.log(`c1 = ${c1}`);
  console.log(`c2 = ${c2}`);
  console.log(`c3 = ${c3}`);
  console.log(`c4 = ${c4}`);
}

function2();
