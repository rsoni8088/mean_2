const express = require("express");

const route = express.Router();

route.get("/", (request, response) => {
  console.log("Inside / GET Method");
  response.end("Dummy Data GET /");
});

route.post("/", (request, response) => {
  console.log("Inside / POST Method");
  response.end("Dummy Data GET /r");
});

route.put("/", (request, response) => {
  console.log("Inside / PUT Method");
  response.end("Dummy Data GET /");
});

route.delete("/", (request, response) => {
  console.log("Inside / DELETE Method");
  response.end("Dummy Data GET /");
});

module.exports = route;
