const express = require("express");

const route = express.Router();

route.get("/", (request, response) => {
  console.log("Inside /user GET Method");
  response.end("Dummy Data GET /user")
});

route.post("/", (request, response) => {
  console.log("Inside /user POST Method");
  response.end("Dummy Data POST /user")
});

route.put("/", (request, response) => {
  console.log("Inside /user PUT Method");
  response.end("Dummy Data PUT /user")
});

route.delete("/", (request, response) => {
  console.log("Inside /user DELETE Method");
  response.end("Dummy Data DELETE /user")
});

module.exports = route;
