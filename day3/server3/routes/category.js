const express = require("express");

const route = express.Router();

const categories = [
  {
    id: "1",
    title: " title 1",
    description: "description 1",
  },
  {
    id: "2",
    title: " title 2",
    description: "description 2",
  },
  {
    id: "3",
    title: " title 3",
    description: "description 3",
  },
];

route.get("/", (request, response) => {
  console.log("Inside /category GET Method");

  // Change category array into string then only we can send send this as text
  // but if we want to send them as JSON then we need to send in JSON format
  //   response.setHeader("Content-type", "application/json");
  //   const strCategories = JSON.stringify(categories);
  // response.end(strCategories);

  // Short cut for this
  response.send(categories);
});

route.post("/", (request, response) => {
  console.log("Inside /category POST Method");

  const { id, title, description } = request.body;

  categories.push({
    id: id,
    title: title,
    description: description,
  });

  response.send("Dummy Data POST /category");
});

route.put("/", (request, response) => {
  console.log("Inside /category PUT Method");
  response.end("Dummy Data PUT /category");
});

route.delete("/", (request, response) => {
  console.log("Inside /category DELETE Method");
  response.end("Dummy Data DELETE /category");
});

module.exports = route;
