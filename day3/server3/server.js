const express = require("express");
const bodyParser = require("body-parser");

const app = express();

const categoryRouter = require("./routes/category");
const userRouter = require("./routes/user");
const indexRouter = require("./routes/index");

app.use(bodyParser.json())

app.use("/category", categoryRouter);
app.use("/user", userRouter);
app.use("/", indexRouter);

app.listen(4000, "0.0.0.0", () => {
  console.log("server started on port 4000");
});
