const http = require("http");

// console.log(http.STATUS_CODES)
// console.log("--------------------------------------")
// console.log(http.METHODS)

function sendHTMLData() {
  response.setHeader("Content-type", "text/html");
}

const server = http.createServer((request, response) => {
  console.log("server has received request");

  function sendHTMLData() {
    const h1 = "<h1>Hi this is my data</h1>";
    response.setHeader("Content-type", "text/html");
    response.end(h1);
  }

  function sendJSONData() {
    const persons = [
      {
        name: "rishabh",
        age: 28,
        address: "pune",
      },
      {
        name: "Sunny",
        age: 24,
        address: "raisen",
      },
      {
        name: "Jyoti",
        age: 28,
        address: "pune",
      },
    ];
    
    response.setHeader("Content-type", "application/json");
    const jsonPersons = JSON.stringify(persons);
    response.end(jsonPersons);
  }
  sendHTMLData();
  //   sendJSONData();
});

server.listen(4000, "0.0.0.0", () => {
  console.log("server started on port 4000");
});
