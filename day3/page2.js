const express = require("http");

const app = express.createServer((request, response) => {
  const path = request.url;
  const method = request.method;

  if (path == "/category") {
    if (method == "GET") {
      console.log("inside /GET");
    } else if (method == "POST") {
      console.log("inside /POST");
    } else if (method == "PUT") {
      console.log("inside /PUT");
    } else if (method == "DELETE") {
      console.log("inside /DELETE");
    }
  }
  response.end("Sending a dummy data");
});

app.listen(4000, "0.0.0.0", () => {
  console.log("server started at port 4000");
});
