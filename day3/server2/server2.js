const { request, response } = require("express");
const express = require("express");

const app = express();

function log1(request, response, next) {
  console.log("Inside log 1");
  console.log("--------------------");
  next();
}

const log2 = (request, response, next) => {
  console.log("inside log 2");
  next();
};

app.use((request, response, next) => {
  console.log("Inside log3");
  next();
});

app.use(log1);
app.use(log2);


app.get("/", (request, response) => {
  console.log("Inside / GET method");
  response.end("Dummy response");
});

app.listen(4000, "0.0.0.0", () => {
  console.log("server started on 4000");
});
