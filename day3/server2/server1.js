const express = require("express");

const app = express();
// middle ware function
function log(request, response, next) {
  console.log("Inside Logger");
  console.log(`Path is ${request.url} , Method is ${request.method}`);
  console.log("----------------------------");

  next()
}


app.use(log)

app.get("/", (request, response) => {
  //   log(request, response);
  response.end(`response from /GET method`);
});

app.post("/", (request, response) => {
  //   log(request, response);
  response.end(`response from /POST method`);
});

app.put("/", (request, response) => {
  //   log(request, response);
  response.end(`response from /PUT meth,responseod`);
});

app.delete("/", (request, response) => {
  //   log(request, response);
  response.end(`response from /DELETE method`);
});

app.get("/category", (request, response) => {
  //   log(request, response);
  response.end(`response category from /GET method`);
});

app.post("/category", (request, response) => {
  //   log(request, response);
  response.end(`response category from /POST method`);
});

app.put("/category", (request, response) => {
  //   log(request, response);
  response.end(`response category from /PUT method`);
});

app.delete("/category", (request, response) => {
  // //   log(request, response);
  response.end(`response category from /DELETE method`);
});
app.listen(4000, "0.0.0.0", () => {
  console.log("Server started on 4000");
});
