const express = require("express");

const categoryRoute = require("./routes/category");

const app = express();
app.use("/category", categoryRoute);

app.listen(4000, "0.0.0.0", () => {
  console.log("server started on port 4000");
});
