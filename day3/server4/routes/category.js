const express = require("express");
const mysql2 = require("mysql2");

const router = express.Router();

router.get("/", (request, response) => {
  // create the connection to database
  const connection = mysql2.createConnection({
    host: "127.0.0.1",
    user: "root",
    password: "Rishabh",
    port: 3306,
    database: "mydata",
  });

  const sql = "select * from category";
  connection.query(sql, (error, result) => {
    connection.close();
    console.log(result);
    response.send(result);
  });
});

module.exports = router;
