const express = require("express");

const app = express();

app.get("/", (request, response) => {
  //   const method = request.method;
  //   const path = request.url;

  //   console.log(method)
  //   console.log(path)

  console.log("inside /GET method");
  response.end("response from /GET method");
});

app.post("/", (request, response) => {
  console.log("inside /POST method");
  response.end("response from /POST method");
});

app.put("/", (request, response) => {
  console.log("inside /PUT method");
  response.end("response from /PUT method");
});
app.delete("/", (request, response) => {
  console.log("inside /DELETE method");
  response.end("response from /DELETE method");
});

app.get("/category", (request, response) => {
  console.log("inside category /GET method");
  response.end("response category from /GET method");
});

app.post("/category", (request, response) => {
  console.log("inside category /POST method");
  response.end("response  category from /POST method");
});

app.put("/category", (request, response) => {
  console.log("inside /PUT method");
  response.end("response from /PUT method");
});
app.delete("/category", (request, response) => {
  console.log("inside category /DELETE method");
  response.end("response category from /DELETE method");
});
app.listen(4000, "0.0.0.0", () => {});
