import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private httpClient: HttpClient) { }

  getProducts() {
    // Create HTTP request 
    const result = this.httpClient.get('http://localhost:4000/products')

    return result


  }

}
