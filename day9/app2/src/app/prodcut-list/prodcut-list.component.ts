import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-prodcut-list',
  templateUrl: './prodcut-list.component.html',
  styleUrls: ['./prodcut-list.component.css']
})
export class ProdcutListComponent implements OnInit {


  products: any = [];

  constructor(private productService: ProductService) {
    // const productService = new ProductService()
    const result = productService.getProducts()

    // send the requset and get the result
    result.subscribe(response => {
      if (response['status'] == 'SUCCESS') {
        this.products = response['data']
      } else {
        console.log('Error while loading data')
      }
    })
  }

  ngOnInit(): void {
  }

}
