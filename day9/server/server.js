const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors")
const productRouter = require("./routes/products");

const app = express();
app.use(bodyParser.json());
app.use(cors())

app.use("/products", productRouter);

app.listen(4000, "0.0.0.0", () => {
  console.log("Server start listening on port no 4000");
});
