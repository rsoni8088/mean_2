const express = require("express");
const db = require("../db");
const utils = require("../utils");

const routes = express.Router();

routes.get("/", (request, response) => {
  const statement = "Select * from product";
  db.query(statement, (error, data) => {
    response.send(utils.sendResult(error, data));
  });
});

routes.post("/", (request, response) => {
  const { title, description, price } = request.body;
  const statement = `insert into product(title, description, price ) values('${title}' ,'${description}','${price}')`;
  db.query(statement, (error, data) => {
    response.send(utils.sendResult(error, data));
  });
});

module.exports = routes;
