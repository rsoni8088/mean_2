const db = require("mysql2");

const pool = db.createPool({
  host: "localhost",
  user: "root",
  password: "Rishabh",
  database: "userschema",
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
});

module.exports = pool;
