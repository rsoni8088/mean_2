import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sixth',
  templateUrl: './sixth.component.html',
  styleUrls: ['./sixth.component.css']
})
export class SixthComponent implements OnInit {

  // Members to collect user input
  fullName = ''
  address = ''

  phoneNumber = ''
  age = 0
  email = ''



  onSave() {
    console.log(`FullName : ${this.fullName}`)
    console.log(`Address : ${this.address}`)
    console.log(`Phone Number : ${this.phoneNumber}`)
    console.log(`Age : ${this.age}`)
    console.log(`Email : ${this.email}`)
  }

  onCancel() {
    this.fullName = ''
    this.address = ''

    this.phoneNumber = ''
    this.age = 0
    this.email = ''

  }

  constructor() { }

  ngOnInit(): void {
  }

}
