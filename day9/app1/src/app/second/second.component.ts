import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.css']
})
export class SecondComponent implements OnInit {

  products = [
    {
      id: 1,
      title: 'Title 1',
      description: 'Description 1',
      price: 100
    },
    {
      id: 2,
      title: 'Title 2',
      description: 'Description 2',
      price: 200
    },
    {
      id: 3,
      title: 'Title 3',
      description: 'Description 3',
      price: 300
    },
    {
      id: 4,
      title: 'Title 4',
      description: 'Description 4',
      price: 400
    }
  ]

  constructor() {

    // This is useful when we have different schema of products

    // for (const product of this.products) {

    //   for (const key in product) {
    //     if (Object.prototype.hasOwnProperty.call(product, key)) {
    //       const element = product[key];
    //       console.log(element)

    //     }
    //   }
    // }
  }

  ngOnInit(): void {
  }

}
