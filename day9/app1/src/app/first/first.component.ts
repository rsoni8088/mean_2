import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {


  firstName = "Steve"
  address = "USA"
  phone = "+91-9673144923"

  product = {
    title: 'Title 1',
    description: 'Description 1',
    price: 100
  }

  constructor() { }

  ngOnInit(): void {
  }

}
