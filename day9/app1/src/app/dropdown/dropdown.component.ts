import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css']
})
export class DropdownComponent implements OnInit {



  // questions and anser now considerd as attribute binding
  // Now we should used Attribute binding []
  // Same like --- React props
  @Input() question = 'Question----1'
  @Input() answer = 'Answer-----1'

  isExpaned = false


  onToggle() {
    this.isExpaned = !this.isExpaned
  }
  constructor() { }

  ngOnInit(): void {
  }

}
