import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-third',
  templateUrl: './third.component.html',
  styleUrls: ['./third.component.css']
})
export class ThirdComponent implements OnInit {


  backgroundColor = 'green'

  onChangeColor(color) {
    this.backgroundColor = color
  }
  onButtonClick() {
    alert("Button Clicked")
  }

  constructor() { }

  ngOnInit(): void {
  }

}
