function sendSuccess(data) {
  return {
    status: "SUCCESS",
    data: data,
  };
}

function sendError(error) {
  return {
    status: "ERROR",
    error: error,
  };
}

function sendResult(error, data) {
  if (error) {
    return sendError(error);
  } else {
    return sendSuccess(data);
  }
}

module.exports = {
  sendSuccess: sendSuccess,
  sendError: sendError,
  sendResult: sendResult,
};
