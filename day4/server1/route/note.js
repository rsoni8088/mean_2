const { request, response } = require("express");
const express = require("express");
const db = require("../db");
const utils = require("../utils");

const router = express.Router();

router.post("/:userId/", (request, response) => {
  const { content } = request.body;
  const { userId } = request.params;
  const statement = `insert into note (userId, content) values ('${userId}' , '${content}')`;
  db.query(statement, (error, data) => {
    response.send(utils.sendResult(error, data));
  });
});

router.get("/:userId/", (request, response) => {
  const { userId } = request.params;
  const statement = `Select * from note where userid = '${userId}'`;
  db.query(statement, (error, data) => {
    response.send(utils.sendResult(error, data));
  });
});

router.put("/:noteId/", (request, response) => {
  const { content } = request.body;
  const { noteId } = request.params;
  const statement = `update note set content = ${content} where id = '${noteId}'`;
  db.query(statement, (error, data) => {
    response.send(utils.sendResult(error, data));
  });
});

router.delete("/:userId/", (request, response) => {
  const { userId } = request.params;
  const statement = `delete from note where userid = '${userId}'`;
  db.query(statement, (error, data) => {
    response.send(utils.sendResult(error, data));
  });
});

module.exports = router;
