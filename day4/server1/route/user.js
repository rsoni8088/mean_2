const express = require("express");
const encrypt = require("crypto-js");

const mailer = require("../mailer");

const db = require("../db");

const utils = require("../utils");
const { request, response } = require("express");

const router = express.Router();

// Instead of creating connection we can create connection pool
// we can define this method inside another file or module

// function createPool() {
//   const pool = mysql.createPool({
//     host: "localhost",
//     user: "root",
//     password: "Rishabh",
//     database: "userschema",
//     waitForConnections: true,
//     connectionLimit: 10,
//   });
//   return pool;
// }

router.post("/register", (request, response) => {
  const { firstName, lastName, email, password, mobile } = request.body;

  //   const connection = mysql.createConnection({
  //     host: "localhost",
  //     user: "root",
  //     password: "Rishabh",
  //     database: "userschema",
  //     port: 3306,
  //   });

  const encrptPassword = encrypt.MD5(password);
  const sql = `insert into user (firstname,lastname, email, password, mobile )
   values ("${firstName}" , "${lastName}","${email}", "${encrptPassword}","${mobile}")`;

  //   connection.query(sql, (error, result) => {

  db.query(sql, (error, result) => {
    if (error) {
      //   console.log(error);
      //   response.status(500).send("Error Occured");
      response.status(500).send(utils.sendError(error));
    } else {
      console.log(result);
      const subject = "Welcome to MEAN tutorial";
      const body = `
      <h1>Hello User</h1>
      <h2>This is a welcome message</h2>
      `;
      mailer.sendEmail(email, subject, body, (emailError, info) => {
        // response.status(200).send({ status: "Success", data: result });
        response.status(500).send(utils.sendSuccess(result));
      });
    }
  });
});

router.post("/login", (request, response) => {
  console.log(request.body);
  const { email, password } = request.body;

  //   const connection = db.createConnection({
  //     host: "localhost",
  //     user: "root",
  //     password: "Rishabh",
  //     database: "userschema",
  //     port: 3306,
  //   });

  const encrptPassword = encrypt.MD5(password);
  const sql = `select * from user where email = "${email}" and password= "${encrptPassword}"`;
  //   connection.query(sql, (error, result) => {
  db.query(sql, (error, result) => {
    if (error) {
      console.log(error.message);
      response.status(500).send("Error Occured");
    } else {
      if (result == 0) {
        console.log("User doesn't exist");
        response.status(500).send({ status: "ERROR", error: error });
      } else {
        console.log(result);
        const user = result[0];
        response.status(200).send({ status: "SUCCESS", data: user });
      }
    }
  });
});

//  /user/profile/1
router.get("/profile/:id", (request, response) => {
  const { id } = request.params;
  const statement = `SELECT id, firstname, lastname, email, mobile from user where id = '${id}'`;
  db.query(statement, (error, result) => {
    if (result.length == 0) {
      response.send(utils.sendError(`No USER with that id = ${id}`));
    } else {
      const user = result[0];
      response.send(utils.sendResult(error, user));
    }
  });
});

//  /user/profile/1
router.put("/profile/:id", (request, response) => {
  const { id } = request.params;
  const { firstName, lastName, mobile } = request.body;
  console.log(id);
  const statement = `update user set firstname = "${firstName}", lastName = "${lastName}" ,mobile= "${mobile}" where id = ${id}`;
  db.query(statement, (error, result) => {
    response.send(utils.sendResult(error, result));
  });
});

module.exports = router;
