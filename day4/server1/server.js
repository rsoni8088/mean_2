const express = require("express");
const bodyParser = require("body-parser")

const userRouter = require("./route/user");
const noteRouter = require("./route/note");
const db = require("./db")

const app = express();
app.use(bodyParser.json());


app.use("/user", userRouter);
app.use("/note", noteRouter);



app.listen(4000, "0.0.0.0", () => {
  console.log("Server started on port 4000");
});
