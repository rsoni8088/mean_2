function function1() {
  const crypto = require("crypto-js");

  const test = "test";

  console.log(`Encrypted password ${crypto.SHA1(test)}`);
  console.log(`Encrypted password ${crypto.SHA3(test)}`);
  console.log(`Encrypted password ${crypto.SHA256(test)}`);
  console.log(`Encrypted password ${crypto.SHA512(test)}`);
  console.log(`Encrypted password ${crypto.MD5(test)}`);
}

// function1()

function function2() {
  const nodemailer = require("nodemailer");

  let mailTransporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "rsoni8088@gmail.com",
      pass: "Jyoti@129",
    },
  });

  let mailDetails = {
    from: "rsoni8088@gmail.com",
    to: "rsoni8088@outlook.com",
    subject: "Test mail",
    text: "Node.js testing mail for GeeksforGeeks",
  };

  mailTransporter.sendMail(mailDetails, function (err, data) {
    if (err) {
      console.log("Error Occurs", err);
    } else {
      console.log("Email sent successfully", data.response);
    }
  });
}

// function2();

function function3() {
  const jwt = require("jsonwebtoken");

  const data = { id: 1 };
  const token = jwt.sign(
    data,
    "1383248931748139038dgwscbdsbcygvcgywgd2e7de78wdgsx"
  );
  console.log("Data", data);
  console.log("Token", token);

  const serverDate = jwt.verify(
    token,
    "1383248931748139038dgwscbdsbcygvcgywgd2e7de78wdgsx"
  );

  console.log(serverDate)
}

function3();
