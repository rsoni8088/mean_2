// Inheritence 
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Person3 = /** @class */ (function () {
    function Person3(_name, _age, _address) {
        this._name = _name;
        this._age = _age;
        this._address = _address;
    }
    Person3.prototype.printInfo = function () {
        console.log("Name : " + this._name + " , Age :" + this._age + " , Address : " + this._address);
    };
    return Person3;
}());
var Employee3 = /** @class */ (function (_super) {
    __extends(Employee3, _super);
    function Employee3(_id, name, age, address) {
        var _this = _super.call(this, name, age, address) || this;
        _this._id = _id;
        return _this;
    }
    Object.defineProperty(Employee3.prototype, "id", {
        set: function (id) {
            this._id = id;
        },
        enumerable: false,
        configurable: true
    });
    // Method over riding
    // Method over loading doesnot supported by JS
    Employee3.prototype.printInfo = function () {
        console.log("Name : " + this._name + " , Id of Employee " + this._id + ", Age :" + this._age + " , Address : " + this._address);
    };
    return Employee3;
}(Person3));
var p3 = new Person3('Rishabh', 28, 'Pune');
p3.printInfo();
console.log("----------------------------------");
var e3 = new Employee3(2, "Jyoti", 28, "Bhopal");
// e2.id = 1;
e3.printInfo();
