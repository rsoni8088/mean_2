// Inheritence 

class Person2 {

    constructor(
        protected _name: string,
        protected _age: number,
        protected _address: string) {

    }

    printPersonInfo() {
        console.log(`Name : ${this._name} , Age :${this._age} , Address : ${this._address}`)
    }
}

class Employee extends Person2 {

    constructor(
        protected _id: number,
        name: string,
        age: number,
        address: string
    ) {
        super(name, age, address);
    }

    public set id(id: number) {
        this._id = id
    }

    printEmployeeInfo() {
        console.log(`Name : ${this._name} , Id of Employee ${this._id}, Age :${this._age} , Address : ${this._address}`)
    }

}

const p2 = new Person2('Rishabh', 28, 'Pune')
p2.printPersonInfo()
console.log("----------------------------------")

const e2 = new Employee(2, "Jyoti", 28, "Bhopal")
// e2.id = 1;
e2.printEmployeeInfo()