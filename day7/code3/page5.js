var Human5 = /** @class */ (function () {
    function Human5() {
    }
    Human5.prototype.runSlow = function () {
        console.log("Human is now running slow");
    };
    Human5.prototype.runFast = function () {
        console.log("Human is now running fast");
    };
    return Human5;
}());
var Animal5 = /** @class */ (function () {
    function Animal5() {
    }
    Animal5.prototype.runSlow = function () {
        console.log("Animal is now running slow");
    };
    Animal5.prototype.runFast = function () {
        console.log("Animal is now running fast");
    };
    return Animal5;
}());
function run(object) {
    object.runSlow();
    object.runFast();
}
var h5 = new Human5();
run(h5);
var a5 = new Animal5();
run(a5);
