// Array 
function function1() {
    const numbers = [10, 20, 30, 40, 50]
    console.log(numbers)

    numbers.push(60)
    console.log(numbers)

    numbers.splice(3, 1)
    console.log(numbers)

    const number2 = numbers.map((number) => {
        return number + 100
    })
    console.log(number2)

    const number3 = numbers.filter(value => {
        return value > 30
    })
    console.log(number3)

    const number4 = numbers.reduce((p1, p2) => {
        return p1 + p2
    })
    console.log(number4)
}


function1()



function function2() {

    // Implicit declaration of any Array
    const numbers: number[] = [10, 20, 30, 40, 50]
    console.log(numbers)

    numbers.push(60)
    console.log(numbers)

    numbers.splice(3, 1)
    console.log(numbers)
}

// function2()
