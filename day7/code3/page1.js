// Aggregation 
var Address = /** @class */ (function () {
    function Address(_city, _state, _country) {
        this._city = _city;
        this._state = _state;
        this._country = _country;
    }
    Address.prototype.printAddress = function () {
        console.log("Address is city - " + this._city + " , State " + this._state + " and Country is " + this._country);
    };
    return Address;
}());
// Person has address
var Person = /** @class */ (function () {
    function Person(_name, _address) {
        this._name = _name;
        this._address = _address;
    }
    Person.prototype.printInfo = function () {
        console.log("Name : " + this._name);
        this._address.printAddress();
    };
    return Person;
}());
var House = /** @class */ (function () {
    function House(_address, _rooms) {
        this._address = _address;
        this._rooms = _rooms;
    }
    House.prototype.printHouseInfo = function () {
        console.log("House has rooms " + this._rooms);
        this._address.printAddress();
    };
    return House;
}());
var p1 = new Person("Rishabh", new Address("Pune", "MH", "India"));
p1.printInfo();
console.log("-----------------------------------------------------");
var h1 = new House(new Address("Mumbai", "MH", "India"), 10);
h1.printHouseInfo();
