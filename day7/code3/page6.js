// Array 
function function1() {
    var numbers = [10, 20, 30, 40, 50];
    console.log(numbers);
    numbers.push(60);
    console.log(numbers);
    numbers.splice(3, 1);
    console.log(numbers);
    var number2 = numbers.map(function (number) {
        return number + 100;
    });
    console.log(number2);
    var number3 = numbers.filter(function (value) {
        return value > 30;
    });
    console.log(number3);
    var number4 = numbers.reduce(function (p1, p2) {
        return p1 + p2;
    });
    console.log(number4);
}
function1();
function function2() {
    // Implicit declaration of any Array
    var numbers = [10, 20, 30, 40, 50];
    console.log(numbers);
    numbers.push(60);
    console.log(numbers);
    numbers.splice(3, 1);
    console.log(numbers);
}
// function2()
