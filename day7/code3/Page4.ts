abstract class Animal {
    public abstract walk();
    public eat() {
        console.log("Animal is eating")
    }
}

class Lion extends Animal {
    public walk() {
        console.log("Lion is walking")
    }

}

class Human extends Animal {
    public walk() {
        console.log("Human is walking")
    }

    public eat() {
        console.log("Human is eating")
    }

}

const l1 = new Lion()
l1.walk();
l1.eat()

const human1 = new Human()
human1.walk();
human1.eat()