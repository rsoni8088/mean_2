// Inheritence 
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Person2 = /** @class */ (function () {
    function Person2(_name, _age, _address) {
        this._name = _name;
        this._age = _age;
        this._address = _address;
    }
    Person2.prototype.printPersonInfo = function () {
        console.log("Name : " + this._name + " , Age :" + this._age + " , Address : " + this._address);
    };
    return Person2;
}());
var Employee = /** @class */ (function (_super) {
    __extends(Employee, _super);
    function Employee(_id, name, age, address) {
        var _this = _super.call(this, name, age, address) || this;
        _this._id = _id;
        return _this;
    }
    Object.defineProperty(Employee.prototype, "id", {
        set: function (id) {
            this._id = id;
        },
        enumerable: false,
        configurable: true
    });
    Employee.prototype.printEmployeeInfo = function () {
        console.log("Name : " + this._name + " , Id of Employee " + this._id + ", Age :" + this._age + " , Address : " + this._address);
    };
    return Employee;
}(Person2));
var p2 = new Person2('Rishabh', 28, 'Pune');
p2.printPersonInfo();
console.log("----------------------------------");
var e2 = new Employee(2, "Jyoti", 28, "Bhopal");
// e2.id = 1;
e2.printEmployeeInfo();
