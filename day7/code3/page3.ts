
// Inheritence 

class Person3 {

    constructor(
        protected _name: string,
        protected _age: number,
        protected _address: string) {

    }

    printInfo() {
        console.log(`Name : ${this._name} , Age :${this._age} , Address : ${this._address}`)
    }
}

class Employee3 extends Person3 {

    constructor(
        protected _id: number,
        name: string,
        age: number,
        address: string
    ) {
        super(name, age, address);
    }

    public set id(id: number) {
        this._id = id
    }
    // Method over riding
    // Method over loading doesnot supported by JS
    printInfo() {
        console.log(`Name : ${this._name} , Id of Employee ${this._id}, Age :${this._age} , Address : ${this._address}`)
    }

}

const p3 = new Person3('Rishabh', 28, 'Pune')
p3.printInfo()
console.log("----------------------------------")

const e3 = new Employee3(2, "Jyoti", 28, "Bhopal")
// e2.id = 1;
e3.printInfo()