// Aggregation 

class Address {

    constructor(
        private _city: string,
        private _state: string,
        private _country: string) {

    }

    printAddress() {
        console.log(`Address is city - ${this._city} , State ${this._state} and Country is ${this._country}`)
    }
}

// Person has address
class Person {
    constructor(
        private _name: string,
        private _address: Address) {

    }

    printInfo() {
        console.log(`Name : ${this._name}`)
        this._address.printAddress()

    }
}

class House {

    constructor(private _address: Address, private _rooms: number) {

    }

    printHouseInfo() {
        console.log(`House has rooms ${this._rooms}`)
        this._address.printAddress()
    }
}

const p1 = new Person("Rishabh", new Address("Pune", "MH", "India"))
p1.printInfo()

console.log("-----------------------------------------------------")

const h1 = new House(new Address("Mumbai", "MH", "India"), 10);
h1.printHouseInfo()
