interface Runnable {
    runSlow();
    runFast();
}

class Human5 implements Runnable {
    runSlow() {
        console.log("Human is now running slow")
    }
    runFast() {
        console.log("Human is now running fast")
    }

}

class Animal5 implements Runnable {
    runSlow() {
        console.log("Animal is now running slow")
    }
    runFast() {
        console.log("Animal is now running fast")
    }
}

function run(object: Runnable) {
    object.runSlow()
    object.runFast()
}

const h5 = new Human5();
run(h5)


const a5 = new Animal5();
run(a5)