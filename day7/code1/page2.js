function function1() {
  let num = 100;
  console.log(`Number is = ${num} and type is = ${typeof num}`);

  num = "Test";
  console.log(`Number is = ${num} and type is = ${typeof num}`);

  num = true;
  console.log(`Number is = ${num} and type is = ${typeof num}`);

  num = { name: "Num 1" };
  console.log(`Number is = ${num} and type is = ${typeof num}`);

  num = 200;
  console.log(`Number is = ${num} and type is = ${typeof num}`);
}

// function1()

function add(p1, p2) {
  console.log(`Addition is ${p1} + ${p2}  = ${p1 + p2}`);
}

add(10, 20);
add(10, '20');