function addition(P1, P2) {
    console.log(`ADDITION IS = ${P1} + ${P2} = ${P1 + P2}`)
}

addition(10, 20)
addition(10, '20')

function addKaro(p1: number, p2: number) {
    console.log(`ADDITION IS = ${p1} + ${p2} = ${p1 + p2}`)
}

addKaro(10, 20)
// Error because of the p2 is number type
// addKaro(10, '20')



function multiply(p1: number, p2: number): number {

    // '' return type is inferred in that case
    return p1 * p2;
}

const multiplication = multiply(4, 5)
console.log(`multiplication IS =${multiplication}`)

const answer = multiplication + 100;
console.log(`answer is = ${answer}`)


const divide = (p1: number, p2: number) => {
    console.log(`divide is = ${p1 / p2}`)
}
divide(10, 2)