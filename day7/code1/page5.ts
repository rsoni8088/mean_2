function function1() {

    // not known the value at the time of declaration
    let myVar: unknown;
    myVar = 100
    myVar = "Name"
    myVar = true
    myVar = undefined


}

function function2(): never {
    throw new Error("This is not return anything");
}

function2()


function function3() {
    enum Color {
        red, green, blue, white, orange
    }

    const red: Color = Color.red
    const green: Color = Color.green
    const blue: Color = Color.blue
    const white: Color = Color.white
    const orange: Color = Color.orange

}


function function4() {
    enum response {
        success = 1, error = 0
    }

    enum statusCode {
        FileNotFound = 404, OK = 200, BedRequest = 400
    }


}