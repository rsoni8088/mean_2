function function1() {
    // type inference

    var num = 100;
    console.log(`Number is = ${num} and type is = ${typeof num}`);
    // Inside TS this will give error
    // num = "String";
    // num = true;

    //String
    let firstName = 'Steve'
    let lastName = 'Jobs'
    let address = 'USA'
    console.log(`firstName is = ${firstName} and type is = ${typeof (firstName)}`);

    let canVote = true
    console.log(`canVote is = ${canVote} and type is = ${typeof (canVote)}`);

    let person = {
        name: 'num 1',
        age: 20
    }
    console.log(`person is = ${person.name} and type is = ${typeof (person)}`);


}

// function1()


function function_two() {
    // Explict declaration

    let num: number = 100;

    let firstName: String = 'Steve'
    let lastName: String = 'Jobs'
    let Address: String = 'USA'

    let canVote: boolean = false

    let person1: Object = {
        name: 'person 1', age: 20, address: 'India'
    }
    let mobile: { model: String, company: String, price: number } = {
        model: 'Iphone XS', company: 'Apple', price: 144000
    }

}

// function2()

function function_3() {
    let response: String | number = 100
    response = "Ok"
    // Not allowed because union supports only String and number only
    // response = true
    // response ={ name : 'Name 1'}

    let response2: String | number | boolean | Object = 200
    response2 = "Ok"
    response2 = true
    response2 = { name: 'Name 2' }
    // Error
    // response3 = [{}, {}]

    let response3: any = 200
    response3 = "Ok"
    response3 = true
    response3 = { name: 'Name 3' }
    response3 = [{}, {}]

}

function_3()