function function1() {
    // type inference
    var num = 100;
    console.log("Number is = " + num + " and type is = " + typeof num);
    // Inside TS this will give error
    // num = "String";
    // num = true;
    //String
    var firstName = 'Steve';
    var lastName = 'Jobs';
    var address = 'USA';
    console.log("firstName is = " + firstName + " and type is = " + typeof (firstName));
    var canVote = true;
    console.log("canVote is = " + canVote + " and type is = " + typeof (canVote));
    var person = {
        name: 'num 1',
        age: 20
    };
    console.log("person is = " + person.name + " and type is = " + typeof (person));
}
// function1()
function function_two() {
    // Explict declaration
    var num = 100;
    var firstName = 'Steve';
    var lastName = 'Jobs';
    var Address = 'USA';
    var canVote = false;
    var person1 = {
        name: 'person 1', age: 20, address: 'India'
    };
    var mobile = {
        model: 'Iphone XS', company: 'Apple', price: 144000
    };
}
// function2()
function function_3() {
    var response = 100;
    response = "Ok";
    // Not allowed because union supports only String and number only
    // response = true
    // response ={ name : 'Name 1'}
    var response2 = 200;
    response2 = "Ok";
    response2 = true;
    response2 = { name: 'Name 2' };
    // Error
    // response3 = [{}, {}]
    var response3 = 200;
    response3 = "Ok";
    response3 = true;
    response3 = { name: 'Name 3' };
    response3 = [{}, {}];
}
function_3();
