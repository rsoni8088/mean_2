class Person3 {

    // properties
    name: string;
    age: number;
    address: string;

    // Method
    printInfo() {
        console.log(`Name ${this.name}, Age ${this.address} , Address ${this.address}`)
    }
}


const p1 = new Person3();
p1.name = "Rishabh"
p1.age = 20
p1.address = "Pune"
p1.printInfo()


const p2 = new Person3()
p1.name = "Jyoti"
p1.age = 20
p1.address = "Bhopal"
p1.printInfo()