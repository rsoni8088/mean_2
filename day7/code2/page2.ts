class Person {

    // Field
    name: string;
    age: number;
    address: string;

    printInfo() {
        console.log(`Person1 name is = ${this.name}, age = ${this.age}, address ${this.address}`);
    }
}