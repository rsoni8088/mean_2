var Person4 = /** @class */ (function () {
    // Methods
    //   - construtor
    //  - desctructor
    //  - getter
    //  - setter (Mutator)
    //  -facilitator
    // Constructor
    function Person4(name, age, address) {
        if (name === void 0) { name = ''; }
        if (age === void 0) { age = 0; }
        if (address === void 0) { address = ''; }
        console.log("Inside constructor");
        this.name = name;
        this.age = age;
        this.address = address;
    }
    // Method
    Person4.prototype.printInfo = function () {
        console.log("Name " + this.name + ", Age " + this.address + " , Address " + this.address);
    };
    return Person4;
}());
var person1 = new Person4('Rishabh', 30, 'Pune');
person1.printInfo();
var person2 = new Person4();
person2.printInfo();
