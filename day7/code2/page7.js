var Person7 = /** @class */ (function () {
    // Constructor
    function Person7(_name, _age, _address) {
        if (_name === void 0) { _name = ''; }
        if (_age === void 0) { _age = 0; }
        if (_address === void 0) { _address = ''; }
        this._name = _name;
        this._age = _age;
        this._address = _address;
    }
    Object.defineProperty(Person7.prototype, "name", {
        // Accessors
        get: function () {
            return this._name;
        },
        set: function (name) {
            this._name = name;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Person7.prototype, "address", {
        get: function () {
            return this._address;
        },
        set: function (address) {
            this._address = address;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Person7.prototype, "age", {
        get: function () {
            return this._age;
        },
        set: function (age) {
            this._age = age;
        },
        enumerable: false,
        configurable: true
    });
    // Method
    Person7.prototype.printInfo = function () {
        console.log("Name " + this._name + ", Age " + this._age + " , Address " + this._address);
    };
    return Person7;
}());
var p7 = new Person7('Rishabh', 30, 'Pune');
p7.name = "Jyoti";
p7.printInfo();
