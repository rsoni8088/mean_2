var Person5 = /** @class */ (function () {
    // Constructor
    function Person5(name, age, address) {
        console.log("Inside constructor");
        this.name = name;
        this.age = age;
        this.address = address;
    }
    // Methods
    //   - construtor
    //  - desctructor
    //  - getter
    //  - setter (Mutator)
    //  -facilitator
    // getter
    Person5.prototype.getName = function () {
        return this.name;
    };
    Person5.prototype.getAddress = function () {
        return this.address;
    };
    Person5.prototype.getAge = function () {
        return this.age;
    };
    //setter
    Person5.prototype.setName = function (name) {
        this.name = name;
    };
    Person5.prototype.setAddress = function (address) {
        this.address = address;
    };
    Person5.prototype.setAge = function (age) {
        if ((age > 0) || (age < 60)) {
            this.age = age;
        }
        else {
            throw new Error("Invalid Age");
        }
    };
    // Method
    Person5.prototype.printInfo = function () {
        console.log("Name = " + this.name + ", Age = " + this.age + " , Address = " + this.address);
    };
    return Person5;
}());
var p5 = new Person5('Person5', 20, 'Pune');
// p5.age = -30
p5.setAge(30);
p5.printInfo();
