class Person4 {
    // properties
    name: string;
    age: number;
    address: string;

    // Methods
    //   - construtor
    //  - desctructor
    //  - getter
    //  - setter (Mutator)
    //  -facilitator


    // Constructor
    constructor(name: string = '', age: number = 0, address: string = '') {

        console.log(`Inside constructor`)
        this.name = name
        this.age = age
        this.address = address
    }


    // Method
    printInfo() {
        console.log(`Name ${this.name}, Age ${this.address} , Address ${this.address}`)
    }
}


var person1 = new Person4('Rishabh', 30, 'Pune');
person1.printInfo()


var person2 = new Person4();
person2.printInfo()