class Person6 {
    // properties
    private _name: string;
    private _age: number;
    private _address: string;

    // Accessors
    public get name(): string {
        return this._name
    }
    public get address(): string {
        return this._address
    }
    public get age(): number {
        return this._age
    }


    public set name(name: string) {
        this._name = name
    }

    public set address(address: string) {
        this._address = address
    }

    public set age(age: number) {
        this._age = age
    }
    // Constructor
    constructor(name: string = '', age: number = 0, address: string = '') {
        this._name = name
        this._age = age
        this._address = address
    }


    // Method
    printInfo() {
        console.log(`Name ${this._name}, Age ${this._age} , Address ${this._address}`)
    }
}


var p6 = new Person6('Rishabh', 30, 'Pune');
p6.age = 40
p6.printInfo()
