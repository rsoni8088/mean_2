// Using JSON

const person1 = {
  name: "person 1",
  age: 20,
};

console.log(`Person1 name is = ${person1.name}, age = ${person1.age}`);

// Using Object

const mobile = new Object();
mobile.model = "IPhone";
mobile.company = "Apple";
mobile.price = 144000;
console.log(
  `Mobile name is = ${mobile.model}, Company = ${mobile.company}, Price= ${mobile.price}k`
);

// using Constructor
function Car(model, company, price) {
  this.model = model;
  this.company = company;
  this.price = price;
}

Car.prototype.printInfo = function () {
  console.log(
    `C1 name is = ${this.model}, Company = ${this.company}, Price= ${this.price} lacs`
  );
};

const c1 = new Car("í20", "Hyndai", 7.5);
c1.printInfo();

const c2 = new Car("í10", "Hyndai", 5.5);
c2.printInfo();
const c3 = new Car("Nano", "Tata", 2.5);
c3.printInfo();
