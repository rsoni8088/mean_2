var Person6 = /** @class */ (function () {
    // Constructor
    function Person6(name, age, address) {
        if (name === void 0) { name = ''; }
        if (age === void 0) { age = 0; }
        if (address === void 0) { address = ''; }
        this._name = name;
        this._age = age;
        this._address = address;
    }
    Object.defineProperty(Person6.prototype, "name", {
        // Accessors
        get: function () {
            return this._name;
        },
        set: function (name) {
            this._name = name;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Person6.prototype, "address", {
        get: function () {
            return this._address;
        },
        set: function (address) {
            this._address = address;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Person6.prototype, "age", {
        get: function () {
            return this._age;
        },
        set: function (age) {
            this._age = age;
        },
        enumerable: false,
        configurable: true
    });
    // Method
    Person6.prototype.printInfo = function () {
        console.log("Name " + this._name + ", Age " + this._age + " , Address " + this._address);
    };
    return Person6;
}());
var p6 = new Person6('Rishabh', 30, 'Pune');
p6.age = 40;
p6.printInfo();
