class Person5 {
    // properties
    private name: string;
    private age: number;
    private address: string;

    // Methods
    //   - construtor
    //  - desctructor
    //  - getter
    //  - setter (Mutator)
    //  -facilitator


    // getter
    public getName(): string {
        return this.name;
    }
    public getAddress(): string {
        return this.address;
    }
    public getAge(): number {
        return this.age;
    }

    //setter
    public setName(name: string) {
        this.name = name;
    }
    public setAddress(address: string) {
        this.address = address;
    }
    public setAge(age: number) {
        if ((age > 0) || (age < 60)) {
            this.age = age;
        } else {
            throw new Error("Invalid Age")
        }

    }

    // Constructor
    constructor(name: string, age: number, address: string) {

        console.log(`Inside constructor`)
        this.name = name
        this.age = age
        this.address = address
    }


    // Method
    printInfo() {
        console.log(`Name = ${this.name}, Age = ${this.age} , Address = ${this.address}`)
    }
}


const p5 = new Person5('Person5', 20, 'Pune')
// p5.age = -30
p5.setAge(30)
p5.printInfo()