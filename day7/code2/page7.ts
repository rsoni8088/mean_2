class Person7 {
    
    // Constructor
    constructor(
        private _name: string = '',
        private _age: number = 0,
        private _address: string = '') {
    }

    // Accessors
    public get name(): string {
        return this._name
    }
    public get address(): string {
        return this._address
    }
    public get age(): number {
        return this._age
    }


    public set name(name: string) {
        this._name = name
    }

    public set address(address: string) {
        this._address = address
    }

    public set age(age: number) {
        this._age = age
    }

    // Method
    printInfo() {
        console.log(`Name ${this._name}, Age ${this._age} , Address ${this._address}`)
    }
}


var p7 = new Person7('Rishabh', 30, 'Pune');
p7.name = "Jyoti"
p7.printInfo()

